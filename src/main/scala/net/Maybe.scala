package net

object Maybe {
  final case class Full[A](a: A) extends Maybe[A]
  final case class Empty[A]() extends Maybe[A]
}
sealed trait Maybe[A] { self =>
  def getOrElse(a: A): A = self match {
    case Maybe.Full(a) => a
    case Maybe.Empty() => a
  }

  def map[B](f: A => B): Maybe[B] = self match {
    case Maybe.Full(a) => Maybe.Full( f(a) )
    case Maybe.Empty() => Maybe.Empty[B]()
  }

  def flatMap[B](f: A => Maybe[B]): Maybe[B] = self match {
    case Maybe.Full(a) => f(a)
    case Maybe.Empty() => Maybe.Empty[B]()
  }

  def mapWithFlatMap[B](f: A => B): Maybe[B] =
    flatMap { a => Maybe.Full(f(a)) }
}
