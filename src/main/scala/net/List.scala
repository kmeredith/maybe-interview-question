package net

sealed trait MyList[A]
case class Cons[A](a: A, as: MyList[A]) extends MyList[A]
case class Empty[A]() extends MyList[A]

object MyList {
  def count[A](list: MyList[A]): Int = list match {
    case Cons(_, tail) => 1 + count(tail)
    case Empty() => 0
  }

  def map[A, B](list: MyList[A], f: A => B): MyList[B] = list match {
    case Cons(hd, tail) => Cons(f(hd), map(tail, f))
    case Empty() => Empty[B]()
  }

  def head[A](list: MyList[A]): scala.Option[A] = list match {
    case Cons(hd, _) => Some(hd)
    case Empty() => None
  }

  def maybeTail[A](list: MyList[A]): scala.Option[MyList[A]] = list match {
    case Cons(_, tail) => Some(tail)
    case Empty() => None
  }

  def last[A](list: MyList[A]): scala.Option[A] = list match {
    case Cons(hd, Empty()) => Some(hd)
    case Cons(_, tail) => last(tail)
    case Empty() => None
  }
}