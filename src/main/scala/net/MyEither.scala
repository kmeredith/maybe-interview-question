package net

sealed abstract class Or[A, B] {
  def map[C](f: B => C): Or[A, C]
  def flatMap[C](f: B => Or[A, C]): Or[A, C]
  def leftMap[C](f: A => C): Or[C, B]
  def get: B
  def getOrElse(b: B): B
}
final case class Rightt[A, B](b: B) extends Or[A, B] {
  override def map[C](f: B => C): Or[A, C] = Rightt(f(b))
  override def flatMap[C](f: B => Or[A, C]): Or[A, C] = f(b)
  override def leftMap[C](f: A => C): Or[C, B] = Rightt(b)
  override def get: B = b
  override def getOrElse(other: B): B = b
}
final case class Leftt[A, B](a: A) extends Or[A, B] {
  override def map[C](f: B => C): Or[A, C] = Leftt(a)
  override def flatMap[C](f: B => Or[A, C]): Or[A, C] = Leftt(a)
  override def leftMap[C](f: A => C): Or[C, B] = Leftt(f(a))
  override def get: B = throw new NoSuchElementException(s"Leftt($a")
  override def getOrElse(other: B): B = other
}

object Or {

  def gather[A, B](or1: Or[A, B], or2: Or[A, B]): (List[A], List[B]) =
    (or1, or2) match {
      case (Rightt(a), Rightt(b)) => (Nil, List(a,b))
      case (Leftt(a), Rightt(b)) => (List(a), List(b))
      case (Leftt(a), Leftt(b)) => (List(a, b), Nil)
      case (Rightt(a), Leftt(b)) => (List(b), List(a))
    }



  def traverse[A, B, C](as: List[A])(f: A => Or[B, C]): Or[B, List[C]] =
    as match {
      case Nil => Rightt(Nil)
      case head :: tail =>
        val result: Or[B, C] = f(head)
        result.flatMap { c: C =>
          traverse(tail)(f).map { list: List[C] => c :: list}
        }
    }

  def traverseWithFold[A, B, C](as: List[A])(f: A => Or[B, C]): Or[B, List[C]] =
    as.foldLeft[Or[B, List[C]]](Rightt(Nil)) {
      (acc: Or[B, List[C]], elem: A) =>
        acc.flatMap { cs: List[C] =>
          val result: Or[B, C] = f(elem)
          result.map { c: C =>
            cs ++ List(c)
          }
        }
    }

  def sequence[A, B](as: List[Or[A, B]]): Or[A, List[B]] =
    traverse(as)(identity)

  def sequenceManual[A, B](as: List[Or[A, B]]): Or[A, List[B]] =
    as match {
      case Nil => Rightt(Nil)
      case head :: tail =>
        head.flatMap { b: B =>
          sequence(tail).map { bs: List[B] =>
            b :: bs
          }
        }
    }

}



