package net

object Maybe2 {
  final case class Full[A](a: A) extends Maybe2[A] {
    override def map[B](f: A => B): Maybe2[B] = Full(f(a))
    override def flatMap[B](f: A => Maybe2[B]): Maybe2[B] = f(a)
    override def mapWithFlatMap[B](f: A => B): Maybe2[B] = flatMap { a => Full(f(a)) }
  }
  final case class Empty[A]() extends Maybe2[A] {
    override def map[B](f: A => B): Maybe2[B] = Empty[B]
    override def flatMap[B](f: A => Maybe2[B]): Maybe2[B] = Empty[B]
    override def mapWithFlatMap[B](f: A => B): Maybe2[B] = Empty[B]
  }
}
sealed trait Maybe2[A] {
  def map[B](f: A => B): Maybe2[B]
  def flatMap[B](f: A => Maybe2[B]): Maybe2[B]
  def mapWithFlatMap[B](f: A => B): Maybe2[B]
}
