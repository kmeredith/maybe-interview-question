package net;

import java.util.function.Function;

public interface Option<A> {
    A getOrElse(A defaultValue);
    <T> Option<T> map(Function<A, T> f);
    <T> Option<T> flatMap(Function<A, Option<T>> f);
    static <T> Option<T> just(T t) {
        return new Just(t);
    }
    static <T> Option<T> none(T t) {
        return new Nothing_();
    }
};
final class Just<A> implements Option<A> {
    private final A a;

    @Override
    public String toString() {
        return "Just(" + a.toString() + ")";
    }

    public Just(A input) {
        this.a = input;
    }

    @Override
    public A getOrElse(A defaultValue) {
        return a;
    }

    @Override
    public <T> Option<T> map(Function<A, T> f) {
        return new Just(f.apply(a));
    }

    @Override
    public <T> Option<T> flatMap(Function<A, Option<T>> f) {
        return f.apply(a);
    }
};
final class Nothing_<A> implements Option<A> {
    public Nothing_() {
    }

    @Override
    public String toString() {
        return "Nothing";
    }

    @Override
    public A getOrElse(A defaultValue) {
        return defaultValue;
    }

    @Override
    public  <T> Option<T> map(Function<A, T> f) {
        return new Nothing_();
    }

    @Override
    public <T> Option<T> flatMap(Function<A, Option<T>> f) {
        return new Nothing_();
    }
};